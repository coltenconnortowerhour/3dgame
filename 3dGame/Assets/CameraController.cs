﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public GameObject playerSphere;
	private Rigidbody sphereBod;

	public Vector3 startingOffset;

	// Use this for initialization
	void Start () {
		startingOffset = transform.position - playerSphere.transform.position;
		sphereBod = (Rigidbody)playerSphere.GetComponent ("Rigidbody");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.LookAt (playerSphere.transform);

		Vector3 desiredPosition = playerSphere.transform.position + startingOffset;
		Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * 10);
		transform.position = position;

		float horizontal = Input.GetAxis("Horizontal") * 1500 * Time.deltaTime;
		sphereBod.AddTorque(new Vector3(horizontal,0,0));

		float vertical = Input.GetAxis("Vertical") * 1500 * Time.deltaTime;
		sphereBod.AddTorque(new Vector3(0,0,vertical));
	}
}
