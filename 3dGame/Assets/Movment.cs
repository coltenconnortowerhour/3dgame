﻿using UnityEngine;
using System.Collections;

public class Movment : MonoBehaviour {
	public float jump;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space ) && IsGrounded() ){
			((Rigidbody)this.GetComponent("Rigidbody")).AddForce(new Vector3(0,jump,0));
		
		}
	}

	bool IsGrounded() {
		return Physics.Raycast(transform.position, -Vector3.up, (float)(((SphereCollider)GetComponent("SphereCollider")).bounds.extents.y + 0.1));
	}
}
