﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LapController : MonoBehaviour {
	public GameObject racer;
	public GameObject lapText;
	public GameObject behindFinishLine;

	public bool wentLap = false;

	public int laps;

	// Use this for initialization
	void Start () {
		//Physics.IgnoreCollision(racer.GetComponent<Collider>(), GetComponent<Collider>());
	}
	
	// Update is called once per frame
	void Update () {
		lapText.GetComponent<Text> ().text = "Lap: "+laps+"/3";
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject == racer) {
			if (wentLap) {
				laps++;
			}
			wentLap = false;
		}
	}
}
