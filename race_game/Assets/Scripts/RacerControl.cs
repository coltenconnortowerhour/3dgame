﻿using UnityEngine;
using System.Collections;

public class RacerControl : MonoBehaviour {
	public GameObject camera;

	private Rigidbody rbody;

	void Start () {
		rbody = (Rigidbody) this.GetComponent ("Rigidbody");
	}

	void Update() {
		Vector3 camdiff = this.transform.position - camera.transform.position;
		camdiff.y = 0;
		camdiff.Normalize ();

		float horizontal = Input.GetAxis("Horizontal") * 400 * Time.deltaTime;
		rbody.AddForce (Quaternion.AngleAxis(-90, Vector3.up)*(camdiff) * -horizontal);

		float vertical = Input.GetAxis("Vertical") * 500 * Time.deltaTime;
		if (vertical <0) return;
		rbody.AddForce (camdiff * vertical);
	}
}
