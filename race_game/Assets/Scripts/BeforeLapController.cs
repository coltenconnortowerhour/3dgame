﻿using UnityEngine;
using System.Collections;

public class BeforeLapController : MonoBehaviour {
	public GameObject racer;
	public GameObject finishLine;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject == racer) {
			finishLine.GetComponent<LapController>().wentLap = true;
		}
	}
}
