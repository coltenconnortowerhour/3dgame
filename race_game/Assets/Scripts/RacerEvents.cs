﻿using UnityEngine;
using System.Collections;

public class RacerEvents : MonoBehaviour {
	public GameObject floor;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.R)) {
			Application.LoadLevel(0);
		}
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.collider.gameObject == floor) {
			Application.LoadLevel(0);
		}
	}
}
